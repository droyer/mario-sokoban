namespace MarioSokoban.Enums;

public enum BlockType
{
    Air,
    Wall,
    Crate,
    CrateInTarget,
    Target
}