using System.Drawing;
using LibExec;
using LibExec.Graphics;
using LibExec.Input;

namespace MarioSokoban;

public sealed class Game : Application
{
    public const int Width = 408;
    public const int Height = 408;

    private readonly Size _gameSize = new(Width, Height);
    private Level? _level;
    private Menu? _menu;
    private float _scale;

    private RenderTexture _target = null!;

    public Game()
    {
        Screen.SetSize(_gameSize * 2);
        _menu = new Menu();
    }

    public static Texture SpritesTexture { get; private set; } = null!;

    protected override void Start()
    {
        Screen.SetMinSize(_gameSize);

        SpritesTexture = Texture.Load("Assets/sprites.png");
        _target = RenderTexture.Load(_gameSize.Width, _gameSize.Height);
        _target.Texture.SetFilter(TextureFilter.FilterPoint);
    }

    protected override void Update()
    {
        _scale = MathF.Min((float)Screen.Width / _gameSize.Width, (float)Screen.Height / _gameSize.Height);

        if (Input.IsKeyDown(Key.Escape))
        {
            if (_level != null)
            {
                BackToMenu();
            }
            else if (_menu != null)
            {
                Exit();
            }
        }

        _level?.Update();
        _menu?.Update();
    }

    protected override void Draw()
    {
        _target.Begin();
        Renderer.Clear(Color.Black);

        _level?.Draw();
        _menu?.Draw();

        _target.End();

        Renderer.Begin();
        Renderer.Clear(Color.Black);

        _target.Texture.Draw(
            new RectangleF(0, 0, _target.Texture.Width, -_target.Texture.Height),
            new RectangleF((Screen.Width - _gameSize.Width * _scale) * 0.5f,
                (Screen.Height - _gameSize.Height * _scale) * 0.5f,
                _gameSize.Width * _scale, _gameSize.Height * _scale));

        Renderer.End();
    }

    public void BackToMenu()
    {
        _menu = new Menu();
        _level = null;
    }

    public float GetScale()
    {
        return _scale;
    }

    public void LoadLevel(int num)
    {
        _level = new Level(num);
        _menu = null;
    }
}