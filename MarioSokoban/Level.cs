using System.Drawing;
using System.Numerics;
using LibExec;
using LibExec.Graphics;
using LibExec.Input;
using MarioSokoban.Enums;

namespace MarioSokoban;

public sealed class Level
{
    private const int Width = 12;
    private const int Height = 12;
    public const int MaxLevel = 10;
    private static Level _instance = null!;

    private readonly Block[,] _blocks = new Block[Width, Height];
    private readonly Player _player = new();

    private readonly Color _whiteColor = Color.FromArgb(235, 235, 235);
    private bool _canMove;
    private int _currentNumber;
    private bool _hasWin;
    private int _numOfTarget;

    public Level(int num)
    {
        LoadLevel(num);
    }

    public void Update()
    {
        if (_canMove)
        {
            _player.Update();
            CheckWin();
        }

        if (Input.IsKeyDown(Key.R))
        {
            LoadLevel(_currentNumber);
        }

        if (_hasWin && Input.IsKeyDown(Key.Enter))
        {
            if (_currentNumber == MaxLevel)
            {
                Application.GetInstance<Game>().BackToMenu();
            }
            else
            {
                LoadLevel(_currentNumber + 1);
            }
        }
    }

    public void Draw()
    {
        Renderer.DrawRectangle(new RectangleF(0, 0, Screen.Width, Screen.Height), Color.FromArgb(200, 200, 200));

        foreach (var block in _blocks)
        {
            block.Draw();
        }

        _player.Draw();

        if (_hasWin)
        {
            Renderer.DrawRectangle(new Rectangle(0, 0, Game.Width, Game.Height), Color.FromArgb(200, 20, 20, 20));

            DrawCenterText($"Level {_currentNumber} completed", 40, 150);

            if (_currentNumber == MaxLevel)
            {
                DrawCenterText("Congratulations! All the levels are done.", 18, 210);
            }
            else
            {
                DrawCenterText("Press 'Enter' to go to the next level.", 20, 210);
            }
        }
    }

    public static Block? GetBlock(int x, int y)
    {
        if (x < 0 || y < 0 || x >= Width || y >= Height)
        {
            return null;
        }

        return _instance._blocks[x, y];
    }

    private void DrawCenterText(string text, int fontSize, float positionY)
    {
        var width = Renderer.MeasureText(text, fontSize);
        Renderer.DrawText(text, new Vector2(408 / 2f - width / 2f, positionY), fontSize, _whiteColor);
    }

    private void LoadLevel(int num)
    {
        _instance = this;

        _currentNumber = num;
        _numOfTarget = 0;
        _hasWin = false;
        _canMove = true;

        var image = Resources.LoadImage($"Assets/levels/level{num}.png");
        var colors = Resources.LoadImageColors(image);

        for (var x = 0; x < Width; x++)
        {
            for (var y = 0; y < Height; y++)
            {
                var color = colors[x + y * Width];

                if (color is { R: 127, G: 0, B: 0 })
                {
                    _player.SetPosition(x, y);
                }

                _blocks[x, y] = new Block(x, y, GetBlockType(color));
                if (_blocks[x, y].IsTypeOf(BlockType.Target))
                {
                    _numOfTarget++;
                }
            }
        }

        Resources.UnloadImage(image);
    }

    private void CheckWin()
    {
        var count = _blocks.Cast<Block>().Count(x => x.IsTypeOf(BlockType.CrateInTarget));

        if (count == _numOfTarget)
        {
            _canMove = false;
            Task.Run(async () =>
            {
                await Task.Delay(100);
                _hasWin = true;
            });
        }
    }

    private static BlockType GetBlockType(Color color)
    {
        return color switch
        {
            { R: 0, G: 0, B: 0 } => BlockType.Wall,
            { R: 255, G: 106, B: 0 } => BlockType.Crate,
            { R: 76, G: 255, B: 0 } => BlockType.Target,
            _ => BlockType.Air
        };
    }
}