using System.Drawing;
using MarioSokoban.Enums;

namespace MarioSokoban;

public class Block
{
    private const int Width = 34;
    private const int Height = 34;

    private static Rectangle _wallRectangle;
    private static Rectangle _crateRectangle;
    private static Rectangle _crateInTargetRectangle;
    private static Rectangle _targetRectangle;
    private readonly bool _isTarget;
    private readonly RectangleF _rectangle;
    private readonly int _x;
    private readonly int _y;

    private RectangleF _sourceRectangle;
    private BlockType _type;

    public Block(int x, int y, BlockType type)
    {
        _x = x;
        _y = y;
        _type = type;

        _isTarget = _type == BlockType.Target;

        _wallRectangle = GetSourceRectangle(0, 0);
        _crateRectangle = GetSourceRectangle(1, 0);
        _crateInTargetRectangle = GetSourceRectangle(2, 0);
        _targetRectangle = GetSourceRectangle(3, 0);

        _rectangle = new RectangleF(_x * Width, _y * Height, Width, Height);
        UpdateSourceRectangle();
    }

    public void Draw()
    {
        if (_type == BlockType.Air)
        {
            return;
        }

        Game.SpritesTexture.Draw(_sourceRectangle, _rectangle);
    }

    public bool IsTypeOf(BlockType type)
    {
        return _type == type;
    }

    public bool TryMove(int xDir, int yDir)
    {
        var nextBlock = Level.GetBlock(_x + xDir, _y + yDir);
        if (nextBlock != null && (nextBlock.IsTypeOf(BlockType.Air) || nextBlock.IsTypeOf(BlockType.Target)))
        {
            UpdateType(BlockType.Air);
            nextBlock.UpdateType(BlockType.Crate);
            return true;
        }

        return false;
    }

    private void UpdateType(BlockType type)
    {
        _type = type;
        if (_isTarget && _type == BlockType.Crate)
        {
            _type = BlockType.CrateInTarget;
        }

        if (_isTarget && _type == BlockType.Air)
        {
            _type = BlockType.Target;
        }

        UpdateSourceRectangle();
    }

    private void UpdateSourceRectangle()
    {
        _sourceRectangle = _type switch
        {
            BlockType.Wall => _wallRectangle,
            BlockType.Crate => _crateRectangle,
            BlockType.CrateInTarget => _crateInTargetRectangle,
            BlockType.Target => _targetRectangle,
            _ => _sourceRectangle
        };
    }

    private static Rectangle GetSourceRectangle(int x, int y)
    {
        return new Rectangle(x * Width, y * Height, Width, Height);
    }
}