using System.Drawing;
using System.Numerics;
using LibExec;
using LibExec.Graphics;
using LibExec.Input;

namespace MarioSokoban;

public sealed class Menu
{
    private const int ButtonWidth = 100;
    private const int ButtonHeight = 22;
    private readonly RectangleF[] _buttons = new RectangleF[10];
    private readonly Game _game = Application.GetInstance<Game>();
    private readonly Color _whiteColor = Color.FromArgb(235, 235, 235);
    private RectangleF _mouseRectangle;
    private int _selectedIndex;

    public void Update()
    {
        var scale = _game.GetScale();

        _mouseRectangle = new RectangleF
        {
            X = (Input.MousePosition.X - (Screen.Width - Game.Width * scale) * 0.5f) / scale,
            Y = (Input.MousePosition.Y - (Screen.Height - Game.Height * scale) * 0.5f) / scale,
            Width = 0.1f / scale,
            Height = 0.1f / scale
        };

        _selectedIndex = -1;
        for (var i = 0; i < Level.MaxLevel; i++)
        {
            _buttons[i] = new RectangleF(
                Game.Width / 2f - ButtonWidth / 2f,
                120 + i * ButtonHeight + i * 5,
                ButtonWidth, ButtonHeight);

            if (_mouseRectangle.IntersectsWith(_buttons[i]))
            {
                _selectedIndex = i;
                if (Input.IsMouseButtonDown(MouseButton.Left))
                {
                    _game.LoadLevel(i + 1);
                }
            }

            DrawButton(i);
        }
    }

    public void Draw()
    {
        DrawCenterText("Mario Sokoban", 45, 20);
        DrawCenterText("Press 'R' to restart the level.", 20, 80);

        for (var i = 0; i < Level.MaxLevel; i++)
        {
            Renderer.DrawRectangle(_buttons[i], Color.DimGray);
            if (_selectedIndex == i)
            {
                Renderer.DrawRectangle(_buttons[i], Color.FromArgb(200, 50, 50, 50));
            }

            DrawButton(i);
        }
    }

    private void DrawCenterText(string text, int fontSize, float positionY)
    {
        var width = Renderer.MeasureText(text, fontSize);
        Renderer.DrawText(text, new Vector2(Game.Width / 2f - width / 2f, positionY), fontSize, _whiteColor);
    }

    private void DrawButton(int i)
    {
        var text = $"Level {i + 1}";
        var width = Renderer.MeasureText(text, 20);
        Renderer.DrawText(text,
            new Vector2(Game.Width / 2f - width / 2f,
                120 + i * ButtonHeight + i * 5 + 1),
            20, _whiteColor);
    }
}