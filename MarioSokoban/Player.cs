using System.Diagnostics;
using System.Drawing;
using System.Numerics;
using LibExec.Input;
using MarioSokoban.Enums;

namespace MarioSokoban;

public sealed class Player
{
    private const int Width = 34;
    private const int Height = 34;
    private readonly Rectangle _downRectangle;
    private readonly Rectangle _leftRectangle;
    private readonly Stopwatch _moveTimer = new();

    private readonly Rectangle _rightRectangle;
    private readonly Rectangle _upRectangle;
    private Vector2 _position;
    private Rectangle _sourceRectangle;
    private int _x;
    private int _y;

    public Player()
    {
        _rightRectangle = GetSourceRectangle(0, 1);
        _leftRectangle = GetSourceRectangle(1, 1);
        _upRectangle = GetSourceRectangle(2, 1);
        _downRectangle = GetSourceRectangle(3, 1);

        _sourceRectangle = _rightRectangle;
    }

    public void Update()
    {
        if ((Input.IsKey(Key.D) || Input.IsKey(Key.Right)) && !_moveTimer.IsRunning)
        {
            Move(1, 0);
            _sourceRectangle = _rightRectangle;
        }
        else if ((Input.IsKey(Key.A) || Input.IsKey(Key.Left)) && !_moveTimer.IsRunning)
        {
            Move(-1, 0);
            _sourceRectangle = _leftRectangle;
        }
        else if ((Input.IsKey(Key.W) || Input.IsKey(Key.Up)) && !_moveTimer.IsRunning)
        {
            Move(0, -1);
            _sourceRectangle = _upRectangle;
        }
        else if ((Input.IsKey(Key.S) || Input.IsKey(Key.Down)) && !_moveTimer.IsRunning)
        {
            Move(0, 1);
            _sourceRectangle = _downRectangle;
        }

        if (_moveTimer is { IsRunning: true, ElapsedMilliseconds: > 100 })
        {
            _moveTimer.Stop();
        }
    }

    public void Draw()
    {
        Game.SpritesTexture.Draw(_sourceRectangle, _position);
    }

    public void SetPosition(int x, int y)
    {
        _x = x;
        _y = y;
        _position = new Vector2(x * Width, y * Height);
    }

    private void Move(int xDir, int yDir)
    {
        var nextBlock = Level.GetBlock(_x + xDir, _y + yDir);
        if (nextBlock is null || nextBlock.IsTypeOf(BlockType.Wall))
        {
            return;
        }

        if (nextBlock.IsTypeOf(BlockType.Crate) || nextBlock.IsTypeOf(BlockType.CrateInTarget))
        {
            if (!nextBlock.TryMove(xDir, yDir))
            {
                return;
            }
        }

        SetPosition(_x + xDir, _y + yDir);
        _moveTimer.Restart();
    }

    private static Rectangle GetSourceRectangle(int x, int y)
    {
        return new Rectangle(x * Width, y * Height, Width, Height);
    }
}