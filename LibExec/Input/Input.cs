using System.Numerics;
using Raylib_cs;

namespace LibExec.Input;

public static class Input
{
    public static Vector2 MousePosition
    {
        get => Raylib.GetMousePosition();
        set => Raylib.SetMousePosition((int)value.X, (int)value.Y);
    }

    public static Vector2 MouseDirection => Raylib.GetMouseDelta();
    public static float MouseWheel => Raylib.GetMouseWheelMove();

    public static Vector2 AxisDirection
    {
        get
        {
            var horizontal = IsKey(Key.D) ? 1 : IsKey(Key.A) ? -1 : 0;
            var vertical = IsKey(Key.S) ? 1 : IsKey(Key.W) ? -1 : 0;
            return new Vector2(horizontal, vertical);
        }
    }

    public static bool IsKey(Key key)
    {
        return Raylib.IsKeyDown((KeyboardKey)key);
    }

    public static bool IsKeyDown(Key key)
    {
        return Raylib.IsKeyPressed((KeyboardKey)key);
    }

    public static bool IsKeyUp(Key key)
    {
        return Raylib.IsKeyReleased((KeyboardKey)key);
    }

    public static bool IsMouseButton(MouseButton mouseButton)
    {
        return Raylib.IsMouseButtonDown((Raylib_cs.MouseButton)mouseButton);
    }

    public static bool IsMouseButtonDown(MouseButton mouseButton)
    {
        return Raylib.IsMouseButtonPressed((Raylib_cs.MouseButton)mouseButton);
    }

    public static bool IsMouseButtonUp(MouseButton mouseButton)
    {
        return Raylib.IsMouseButtonReleased((Raylib_cs.MouseButton)mouseButton);
    }
}