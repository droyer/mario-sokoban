using System.Numerics;
using LibExec.Extensions;
using Raylib_cs;

namespace LibExec;

public sealed class Camera
{
    public Vector2 Offset { get; set; }
    public Vector2 Target { get; set; }
    public float Rotation { get; set; }
    public float Zoom { get; set; } = 1;

    public void Begin()
    {
        Raylib.BeginMode2D(this.ToRaylibCamera());
    }

    public void End()
    {
        Raylib.EndMode2D();
    }
}