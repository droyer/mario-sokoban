using System.Drawing;
using LibExec.Graphics;
using Raylib_cs;
using Color = Raylib_cs.Color;
using Image = LibExec.Graphics.Image;
using RayImage = Raylib_cs.Image;
using Rectangle = Raylib_cs.Rectangle;

namespace LibExec.Extensions;

internal static class RaylibExtensions
{
    internal static Color ToRaylibColor(this System.Drawing.Color color)
    {
        return new Color(color.R, color.G, color.B, color.A);
    }

    internal static Color ToRaylibColorOrDefault(this System.Drawing.Color? color)
    {
        return color?.ToRaylibColor() ?? Color.WHITE;
    }

    internal static Rectangle ToRaylibRectangle(this RectangleF rectangle)
    {
        return new Rectangle(rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height);
    }

    internal static Camera2D ToRaylibCamera(this Camera camera)
    {
        return new Camera2D(camera.Offset, camera.Target, camera.Rotation, camera.Zoom);
    }

    internal static Texture ToTexture(this Texture2D texture)
    {
        return new Texture
        {
            Id = texture.id,
            Width = texture.width,
            Height = texture.height,
            Mipmaps = texture.mipmaps,
            Format = (int)texture.format
        };
    }

    internal static Image ToImage(this RayImage image)
    {
        return new Image
        {
            Width = image.width,
            Height = image.height,
            Mipmaps = image.mipmaps,
            Format = (int)image.format
        };
    }
}