using System.Drawing;
using System.Globalization;
using System.Numerics;
using LibExec.Extensions;
using Raylib_cs;
using Color = System.Drawing.Color;

namespace LibExec.Graphics;

public static class Renderer
{
    public static void Begin()
    {
        Raylib.BeginDrawing();
    }

    public static void End()
    {
        Rlgl.rlDrawRenderBatchActive();
        Raylib.SwapScreenBuffer();
    }

    public static void Clear(Color color)
    {
        Raylib.ClearBackground(color.ToRaylibColor());
    }

    public static void DrawFps(Vector2 position, int fontSize, Color color)
    {
        DrawText(Time.Fps.ToString(CultureInfo.InvariantCulture), position, fontSize, color);
    }

    public static void DrawText(string text, Vector2 position, int fontSize, Color color)
    {
        Raylib.DrawText(text, (int)position.X, (int)position.Y, fontSize, color.ToRaylibColor());
    }

    public static int MeasureText(string text, int fontSize)
    {
        return Raylib.MeasureText(text, fontSize);
    }

    public static void DrawRectangle(RectangleF rectangle, Color color)
    {
        Raylib.DrawRectangleRec(rectangle.ToRaylibRectangle(), color.ToRaylibColor());
    }

    public static void BeginCamera(Camera camera)
    {
        Raylib.BeginMode2D(camera.ToRaylibCamera());
    }

    public static void EndCamera()
    {
        Raylib.EndMode2D();
    }

    public static void BeginTextureMode(RenderTexture target)
    {
        var renderTextureInternal = Resources.GetRenderTextureInternal(target);
        Raylib.BeginTextureMode(renderTextureInternal);
    }

    public static void EndTextureMode()
    {
        Raylib.EndTextureMode();
    }

    #region Texture

    public static void DrawTexture(Texture texture, Vector2 position, Color? color = null)
    {
        var internalTexture = Resources.GetTextureInternal(texture);
        Raylib.DrawTextureV(internalTexture, position, color.ToRaylibColorOrDefault());
    }

    public static void DrawTexture(Texture texture, Vector2 position, float rotation, float scale = 1,
        Color? color = null)
    {
        var internalTexture = Resources.GetTextureInternal(texture);
        Raylib.DrawTextureEx(internalTexture, position, rotation, scale, color.ToRaylibColorOrDefault());
    }

    public static void DrawTexture(Texture texture, RectangleF source, RectangleF destination, Vector2? origin = null,
        float rotation = 0, Color? color = null)
    {
        var internalTexture = Resources.GetTextureInternal(texture);
        Raylib.DrawTexturePro(internalTexture, source.ToRaylibRectangle(), destination.ToRaylibRectangle(),
            origin ?? Vector2.Zero, rotation, color.ToRaylibColorOrDefault());
    }

    public static void DrawTexture(Texture texture, RectangleF source, Vector2 position, Color? color = null)
    {
        var internalTexture = Resources.GetTextureInternal(texture);
        Raylib.DrawTextureRec(internalTexture, source.ToRaylibRectangle(), position, color.ToRaylibColorOrDefault());
    }

    #endregion
}