using System.Drawing;
using System.Numerics;
using Raylib_cs;
using Color = System.Drawing.Color;

namespace LibExec.Graphics;

public sealed class Texture
{
    internal Texture()
    {
    }

    public required uint Id { get; init; }
    public required int Width { get; init; }
    public required int Height { get; init; }
    public required int Mipmaps { get; init; }
    public required int Format { get; init; } // todo: use enum

    public static Texture Load(string fileName)
    {
        return Resources.LoadTexture(fileName);
    }

    public void Draw(Vector2 position, Color? color = null)
    {
        Renderer.DrawTexture(this, position, color);
    }

    public void Draw(Vector2 position, float rotation, float scale = 1, Color? color = null)
    {
        Renderer.DrawTexture(this, position, rotation, scale, color);
    }

    public void Draw(RectangleF source, RectangleF destination, Vector2? origin = null, float rotation = 0,
        Color? color = null)
    {
        Renderer.DrawTexture(this, source, destination, origin, rotation, color);
    }

    public void Draw(RectangleF source, Vector2 position, Color? color = null)
    {
        Renderer.DrawTexture(this, source, position, color);
    }

    public void SetFilter(TextureFilter filter)
    {
        var internalTexture = Resources.GetTextureInternal(this);
        Raylib.SetTextureFilter(internalTexture, (Raylib_cs.TextureFilter)filter);
    }
}