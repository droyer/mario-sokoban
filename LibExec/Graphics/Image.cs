namespace LibExec.Graphics;

public sealed class Image
{
    internal Image()
    {
    }

    public required int Width { get; init; }
    public required int Height { get; init; }
    public required int Mipmaps { get; init; }
    public required int Format { get; init; } // todo: use enum
}