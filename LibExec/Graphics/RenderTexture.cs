namespace LibExec.Graphics;

public sealed class RenderTexture
{
    internal RenderTexture()
    {
    }

    public required uint Id { get; init; }
    public required Texture Texture { get; init; }
    public required Texture Depth { get; init; }

    public static RenderTexture Load(int width, int height)
    {
        return Resources.LoadRenderTexture(width, height);
    }

    public void Begin()
    {
        Renderer.BeginTextureMode(this);
    }

    public void End()
    {
        Renderer.EndTextureMode();
    }
}