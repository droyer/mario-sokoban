using LibExec.Extensions;
using Raylib_cs;
using Color = System.Drawing.Color;
using RayImage = Raylib_cs.Image;
using RayColor = Raylib_cs.Color;

namespace LibExec.Graphics;

public sealed class Resources
{
    private static Resources _instance = null!;
    private readonly string _baseDirectory;
    private readonly Dictionary<Image, RayImage> _images = new();
    private readonly Dictionary<RenderTexture, RenderTexture2D> _renderTextures = new();
    private readonly Dictionary<Texture, Texture2D> _textures = new();

    internal Resources()
    {
        _instance = this;
        _baseDirectory = $"{AppDomain.CurrentDomain.BaseDirectory}";
    }

    public static Texture LoadTexture(string fileName)
    {
        var textureInternal = Raylib.LoadTexture(Path.Combine(_instance._baseDirectory, fileName));
        if (!Raylib.IsTextureReady(textureInternal))
        {
            throw new Exception($"Failed to open file '{fileName}'");
        }

        var texture = textureInternal.ToTexture();
        _instance._textures.Add(texture, textureInternal);
        return texture;
    }

    public static RenderTexture LoadRenderTexture(int width, int height)
    {
        var renderTextureInternal = Raylib.LoadRenderTexture(width, height);
        if (!Raylib.IsRenderTextureReady(renderTextureInternal))
        {
            throw new Exception("Failed to load the render texture");
        }

        // todo: use extension method
        var renderTexture = new RenderTexture
        {
            Id = renderTextureInternal.id,
            Texture = renderTextureInternal.texture.ToTexture(),
            Depth = renderTextureInternal.depth.ToTexture()
        };

        _instance._textures.Add(renderTexture.Texture, renderTextureInternal.texture);
        _instance._textures.Add(renderTexture.Depth, renderTextureInternal.depth);

        _instance._renderTextures.Add(renderTexture, renderTextureInternal);

        return renderTexture;
    }

    public static void UnloadRenderTexture(RenderTexture? renderTexture)
    {
        if (renderTexture is null || !_instance._renderTextures.TryGetValue(renderTexture, out var _))
        {
            return;
        }

        Raylib.UnloadRenderTexture(GetRenderTextureInternal(renderTexture));
        _instance._renderTextures.Remove(renderTexture);
    }

    public static Image LoadImage(string fileName)
    {
        var imageInternal = Raylib.LoadImage(fileName);
        if (!Raylib.IsImageReady(imageInternal))
        {
            throw new Exception($"Failed to open file '{fileName}'");
        }

        var image = imageInternal.ToImage();
        _instance._images.Add(image, imageInternal);

        return image;
    }

    public static unsafe Color[] LoadImageColors(Image image)
    {
        var imageInternal = _instance._images[image];

        var colorsInternal = Raylib.LoadImageColors(imageInternal);

        var totalPixels = imageInternal.width * imageInternal.height;
        var colors = new Color[totalPixels];

        for (var i = 0; i < totalPixels; i++)
        {
            // todo: use method extension
            colors[i] = Color.FromArgb(colorsInternal[i].a, colorsInternal[i].r, colorsInternal[i].g,
                colorsInternal[i].b);
        }

        Raylib.UnloadImageColors(colorsInternal);

        return colors;
    }

    public static void UnloadImage(Image image)
    {
        Raylib.UnloadImage(_instance._images[image]);
        _instance._images.Remove(image);
    }

    internal void UnloadTextures()
    {
        foreach (var texture in _textures.Values)
        {
            Raylib.UnloadTexture(texture);
        }
    }

    internal void UnloadRenderTextures()
    {
        foreach (var renderTextures in _renderTextures.Values)
        {
            Raylib.UnloadRenderTexture(renderTextures);
        }
    }

    internal void UnloadImages()
    {
        foreach (var image in _images.Values)
        {
            Raylib.UnloadImage(image);
        }
    }

    internal static Texture2D GetTextureInternal(Texture texture)
    {
        return _instance._textures[texture];
    }

    internal static RenderTexture2D GetRenderTextureInternal(RenderTexture renderTexture)
    {
        return _instance._renderTextures[renderTexture];
    }
}