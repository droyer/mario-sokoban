using LibExec.Graphics;
using Raylib_cs;

namespace LibExec;

public abstract class Application : IDisposable
{
    private static Application _instance = null!;

    private readonly DispatcherFrame _dispatcher = new();

    private readonly Resources _resources = new();
    private readonly Screen _screen = new();
    private bool _isRunning = true;

    protected Application()
    {
        _instance = this;

        _dispatcher.OnUpdate += UpdateInternal;
        _dispatcher.OnDraw += DrawnInternal;
    }

    public virtual void Dispose()
    {
    }

    public void Run()
    {
        _dispatcher.Start();

        Raylib.SetConfigFlags(ConfigFlags.FLAG_WINDOW_RESIZABLE);
        Raylib.SetTraceLogLevel(TraceLogLevel.LOG_WARNING);
        Raylib.InitWindow(Screen.Width, Screen.Height, Screen.Title);

        Raylib.SetExitKey(0);

        Start();

        while (!Raylib.WindowShouldClose() && _isRunning)
        {
            _dispatcher.Update();
        }

        _resources.UnloadTextures();
        _resources.UnloadRenderTextures();
        _resources.UnloadImages();
        Raylib.CloseWindow();
    }

    public static void Exit()
    {
        _instance._isRunning = false;
    }

    public static T GetInstance<T>() where T : Application
    {
        return (T)_instance;
    }

    protected abstract void Start();
    protected abstract void Update();
    protected abstract void Draw();

    private void UpdateInternal()
    {
        Raylib.PollInputEvents();
        _screen.Update();
        Update();
    }

    private void DrawnInternal()
    {
        Draw();
    }
}